import pygame, sys, os
pygame.init()
screen = pygame.display.set_mode((1000, 500))
pygame.display.set_caption("Pham Van Phap")


while (True):
    screen.fill((255, 255, 255))
    for event in pygame.event.get():
        if (event.type == pygame.MOUSEBUTTONDOWN):
            if (event.button == 1): print("Right click")
            else: print("Left click")
        elif (event.type == pygame.QUIT): sys.exit('Byee')
    pygame.display.flip()
pygame.quit()
